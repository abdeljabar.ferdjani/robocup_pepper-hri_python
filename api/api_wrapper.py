import json
import time


class ApiWrapper:
    CANCEL = "cancel"
    GOAL = "goal"
    RESULT = "result"
    STATUS = "status"
    FEEDBACK = "feedback"

    prefix = "R2019"  # will be overided in ApiWrapper init

    channels = {}

    callback_ids = {}

    memory = None

    def __init__(self, memory):
        ApiWrapper.memory = memory
        self._api = {}
        self._load_jsons()

        ApiWrapper.prefix = self._api['common']['prefix']
        self.view = View()
        self.timeBoard = TimeBoard(self._api)

    def _load_jsons(self):
        with open("./common.json") as common_json:
            self._api['common'] = json.loads(common_json)

        with open("./generalManagerToHRI.json") as gm_to_hri_json:
            self._api['gmToHRI'] = json.loads(gm_to_hri_json)

    @staticmethod
    def attach_event(event_name, callback):
        if callback:
            ApiWrapper.callback_ids[event_name] = ApiWrapper.memory.subscriber(event_name)
            ApiWrapper.callback_ids[event_name].signal.connect(callback)

    @staticmethod
    def init_channels(view):
        channels = {}

        for type in [
            ApiWrapper.CANCEL,
            ApiWrapper.FEEDBACK,
            ApiWrapper.GOAL,
            ApiWrapper.RESULT,
            ApiWrapper.STATUS
        ]:
            chan = ApiWrapper.get_almemory_address(ApiWrapper.prefix, type)
            channels[type] = chan
            ApiWrapper.memory.setData(chan, "{}")

        ApiWrapper.channels[view] = channels

    @staticmethod
    def raise_goal(view, goal):
        ApiWrapper.memory.raiseEvent(
            ApiWrapper.get_almemory_address(view, ApiWrapper.prefix, ApiWrapper.GOAL),
            json.dumps(goal))

    @staticmethod
    def wait_view(view, callback):
        ApiWrapper.attach_event(view, callback)

    @staticmethod
    def get_almemory_address(name, prefix, type):
        return "{0}/{1}/{2}".format(prefix, name, type)


# noinspection PyShadowingBuiltins
class View:
    askName = "askName"
    askDrink = "askDrink"
    askAge = "askAge"
    confirm = "confirm"
    goTo = "goTo"
    seatGuest = "seatGuest"
    askToFollow = "askToFollow"
    mainMenu = "mainMenu"
    waitView = "wait"
    askOpenDoor = "askOpenDoor"
    callHuman = "callHuman"
    showVideo = "showVideo"

    def __init__(self):
        self.start = View.Start()
        self.listenAsync = View.Wait()

    class Wait:

        def __init__(self):
            pass

        def ask_name(self, callback):
            """
            Wait the view askName
            :param callback: the callback executed when the channel result get data
            """
            ApiWrapper.wait_view(callback)

        def ask_drink(self, callback):
            """
            Wait the view askDrink
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def ask_age(self, callback):
            """
            Wait the view askAge
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def confirm(self, callback):
            """
            Wait the view confirm
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def detail_drinks(self):
            raise NotImplementedError()

        def present_person(self):
            raise NotImplementedError()

        def go_to(self, callback):
            """
            Wait the view goTo
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def seat_guest(self, callback):
            """
            Wait the view seatGuest
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def ask_to_follow(self, callback):
            """
            Wait the view askToFollow
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def main_menu(self, callback):
            """
            Wait the view mainMenu
            :param callback: the callback executed when the channel result get data
            """
            ApiWrapper.wait_view(callback)

        def wait(self, callback):
            """
            Wait the view wait
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def ask_open_door(self, callback):
            """
            Wait the view askOpenDoor
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def call_human(self, callback):
            """
            Wait the view callHuman
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def show_video(self, callback):
            """
            Wait the view showVideo
            :param callback: the callback executed when the channel result get data
            """

            ApiWrapper.wait_view(callback)

        def find_available_drinks(self):
            raise NotImplementedError()

        def open_door(self):
            raise NotImplementedError()

        def find_who_wants_drinks(self):
            raise NotImplementedError()

        def serve_drinks(self):
            raise NotImplementedError()

    class Start:
        def __init__(self):
            pass

        def ask_name(self, id, speech, names):
            """
            Start the view 'askName'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            :param names: a list of names that can be chosen by operators
            :type names: list
            """
            ApiWrapper.init_channels(View.askName)
            ApiWrapper.raise_goal(View.askName, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                    'names': names
                }
            })

        # region Starter
        def ask_drink(self, id, speech, drinks):
            """
            Start the view 'askDrink'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            :param drinks: a list of drinks that can be chosen by operators
            :type drinks: list
            """
            ApiWrapper.init_channels(View.askDrink)
            ApiWrapper.raise_goal(View.askDrink, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                    'drinks': drinks
                }
            })

        def ask_age(self, id, speech):
            """
            Start the view 'askAge'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            """
            ApiWrapper.init_channels(View.askAge)
            ApiWrapper.raise_goal(View.askAge, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                }
            })

        def confirm(self, id, speech):
            """
            Start the view 'confirm'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            """
            ApiWrapper.init_channels(View.confirm)
            ApiWrapper.raise_goal(View.confirm, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                }
            })

        def detail_drinks(self):
            raise NotImplementedError()

        def present_person(self):
            raise NotImplementedError()

        def go_to(self, id, speech):
            """
            Start the view 'goTo'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            """
            ApiWrapper.init_channels(View.goTo)
            ApiWrapper.raise_goal(View.goTo, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                }
            })

        def seat_guest(self, id, speech, guest):
            """
            Start the view 'seatGuest'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            """
            ApiWrapper.init_channels(View.seatGuest)
            ApiWrapper.raise_goal(View.seatGuest, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                    'guest': guest
                }
            })

        def ask_to_follow(self, id, speech):
            """
            Start the view 'askToFollow'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            """
            ApiWrapper.init_channels(View.askToFollow)
            ApiWrapper.raise_goal(View.askToFollow, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                }
            })

        def main_menu(self, id, speech, scenarios):
            """
            Start the view 'mainMenu'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            """
            ApiWrapper.init_channels(View.mainMenu)
            ApiWrapper.raise_goal(View.mainMenu, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                    'scenarios': scenarios
                }
            })

        def wait(self, id, speech, waiting_time):
            """
                Start the view 'wait'

                :param id: the id of the action
                :type id: int
                :param speech: the text that will be use by the Local Manager for tablet and vocal
                :type speech: dict
            """
            ApiWrapper.init_channels(View.waitView)
            ApiWrapper.raise_goal(View.waitView, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'time': waiting_time,
                    'speech': speech,
                }
            })

        def ask_open_door(self, id, speech):
            """
                Start the view 'askOpenDoor'

                :param id: the id of the action
                :type id: int
                :param speech: the text that will be use by the Local Manager for tablet and vocal
                :type speech: dict
            """
            ApiWrapper.init_channels(View.askOpenDoor)
            ApiWrapper.raise_goal(View.askOpenDoor, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                }
            })

        def call_human(self, id, speech):
            """
                Start the view 'callHuman'

                :param id: the id of the action
                :type id: int
                :param speech: the text that will be use by the Local Manager for tablet and vocal
                :type speech: dict
            """
            ApiWrapper.init_channels(View.callHuman)
            ApiWrapper.raise_goal(View.callHuman, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                }
            })

        def show_video(self, id, speech, steps, video_url):
            """
            Start the view 'showVideo'

            :param id: the id of the action
            :type id: int
            :param speech: the text that will be use by the Local Manager for tablet and vocal
            :type speech: dict
            """
            ApiWrapper.init_channels(View.showVideo)
            ApiWrapper.raise_goal(View.showVideo, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'speech': speech,
                    'steps': steps,
                    'video_url': video_url
                }
            })

        def find_available_drinks(self):
            raise NotImplementedError()

        def open_door(self):
            raise NotImplementedError()

        def find_who_wants_drinks(self):
            raise NotImplementedError()

        def serve_drinks(self):
            raise NotImplementedError()

        # endregion


class TimeBoard:
    def __init__(self):
        pass

    current_step = "currentStep"
    step_done = "stepDone"
    step_skipped = "stepSkipped"
    step_list = "stepsList"

    class Start:

        def __init__(self):
            pass

        def current_step(self, id, step_index):
            """
                Set current a steps

                :param id: the id of the action
                :type id: int
                :param step_index: the index of the step
                :type step_index: int
            """
            ApiWrapper.init_channels(TimeBoard.current_step)
            ApiWrapper.raise_goal(TimeBoard.current_step, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'stepId': step_index,
                }
            })

        def step_done(self, id, step_indexes):
            """
                Set done a list of steps

                :param id: the id of the action
                :type id: int
                :param step_indexes: A list of indexes of the steps
                :type step_indexes: list
            """
            ApiWrapper.init_channels(TimeBoard.step_done)
            ApiWrapper.raise_goal(TimeBoard.step_done, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'stepId': step_indexes,
                }
            })

        def step_skipped(self, id, step_indexes):
            """
                Set skipped a list of steps

                :param id: the id of the action
                :type id: int
                :param step_indexes: A list of indexes of the steps
                :type step_indexes: list
            """
            ApiWrapper.init_channels(TimeBoard.step_skipped)
            ApiWrapper.raise_goal(TimeBoard.step_skipped, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'stepId': step_indexes,
                }
            })

        def steps_list(self, id, step_list):
            """
                Change all steps in timeboard

                :param id: the id of the action
                :type id: int
                :param step_list: A list of new steps
                :type step_list: list
            """
            ApiWrapper.init_channels(TimeBoard.step_list)
            ApiWrapper.raise_goal(TimeBoard.step_list, {
                'id': id,
                'timestamp': time.time(),
                'args': {
                    'stepsList': step_list,
                }
            })


if __name__ == '__main__':
    wrapper = ApiWrapper(memory)


    def do_smth():
        print("It's something time")


    wrapper.view.listenAsync.wait(do_smth)
    wrapper.view.start.wait(
        0, {
            "said": "I'm waiting for 5 seconds !",
            "title": "I'm waiting for 5 seconds"
        }, 5
    )
