import json
from tools import logger
from steps.ask_age import AskAge
from steps.ask_drink import AskDrink
from steps.ask_name import AskName
from steps.ask_open_door import AskOpenDoor
from steps.ask_to_follow import AskToFollow
from steps.call_human import CallHuman
from steps.confirm import Confirm
from steps.generic import Generic
from steps.go_to import GoTo
from steps.present_person import PresentPerson
from steps.seat_guest import SeatGuest
from steps.show_video import ShowVideo
from steps.wait import Wait

class Views:
    last_action = None
    current_scenario = None
    action_class_map = {
        'askAge': AskAge,
        'askDrink': AskDrink,
        'askName': AskName,
        'askOpenDoor': AskOpenDoor, # textToPrint
        'askToFollow': AskToFollow, # 
        'callHuman': CallHuman,
        'confirm': Confirm, # textToPrint
        'detailDrinks': None,
        'findAvailableDrinks': None,
        'findWhoWantsDrinks': None,
        'generic': Generic,
        'goTo': GoTo, # textToPrint
        'openDoor': None,
        'presentPerson': PresentPerson, # textToPrint, people: {who: {name, drinkId}, to: {name, drinkId}}
        'seatGuest': SeatGuest, # 
        'serveDrinks': None,
        'showVideo': ShowVideo,
        'wait': Wait # time
    }

    @staticmethod
    def start(step_name, local_manager, arguments):
        if step_name in Views.action_class_map and Views.action_class_map[step_name]:
            Views.stop(local_manager)
            try:
                Views.last_action = step_name
                logger.log('Loading "' + step_name + '"', "QiChat", logger.DEBUG)
                Views.action_class_map[step_name].start(step_name, local_manager, arguments)
                logger.log('Loaded "' + step_name + '"', "QiChat", logger.DEBUG)
            except RuntimeError as ex:
                logger.log('An error occured while loading "' + step_name + '":\n' + str(ex), "QiChat", logger.ERROR)
                local_manager.send_view_result(step_name, {'id': arguments['id'], 'error': "500"})
                Views.last_action = None
        else:
            logger.log("View " + step_name + "not found, sending result error", "Views Manager", logger.DEBUG)
            local_manager.send_view_result(step_name, {'id': arguments['id'], 'error': "404"})


    @staticmethod
    def received_data(local_manager, data):
        if Views.last_action:
            try:
                logger.log('Received data on "' + str(Views.last_action) + '"', "QiChat", logger.DEBUG)
                result = Views.action_class_map[Views.last_action].received_data(local_manager, data)
                logger.log('Processed data on "' + str(Views.last_action) + '"', "QiChat", logger.DEBUG)

                if result:
                    local_manager.send_view_result(Views.last_action, result)
                    logger.log("Sent on ActionComplete: " + str({'ok': Views.last_action}), "Views Manager", logger.DEBUG)
            except RuntimeError as ex:
                logger.log('An error occured while processing data on "' + Views.last_action + '":\n' + str(ex), "Views Manager", logger.ERROR)
                local_manager.send_view_result(Views.last_action, {'id': Views.last_action.action_id, 'error': "500"})
                

    @staticmethod
    def stop(local_manager):
        if Views.last_action:
            try:
                logger.log('Unloading "' + str(Views.last_action) + '"', "QiChat", logger.DEBUG)
                Views.action_class_map[Views.last_action].stop(local_manager)
                logger.log('Unloaded "' + str(Views.last_action) + '"', "QiChat", logger.DEBUG)
            except RuntimeError as ex:
                logger.log('An error occured while unloading "' + str(Views.last_action) + '":\n' + str(ex), "QiChat", logger.ERROR)
                local_manager.send_view_result(Views.last_action, {'id': Views.action_class_map[Views.last_action].action_id, 'error': "500"})
            Views.last_action = None
