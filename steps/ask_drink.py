import json
import os
from tools import logger
from tools import arg_fetcher


class AskDrink:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        AskDrink.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not AskDrink.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')
        
        AskDrink.drinks = arg_fetcher.get_argument(args, 'drinks')
        
        name = arg_fetcher.get_argument(speech, 'name')
        
        text = text.format(name=name)
        said = said.format(name=name)

        if text:
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                'data': {
                    'textToShow': text,
                    'drinks': AskDrink.drinks
                }
            }))

        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                AskDrink.reactivateMovement = True

            if arg_fetcher.get_argument(speech, 'noSpeechRecognition'):
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['error'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                AskDrink.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(AskDrink.topic_name)

                local_manager.dialog.activateTag("saySmthTag", AskDrink.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", AskDrink.topic_name)

            else:
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['ok'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['ask_smth'])

                local_manager.dialog.setConcept("askSmthDyn", "enu", [said])
                local_manager.dialog.setConcept("choices", "enu", [drink['name'] for drink in AskDrink.drinks])

                AskDrink.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(AskDrink.topic_name)

                local_manager.dialog.activateTag("askSmthTag", AskDrink.topic_name)
                local_manager.dialog.gotoTag("askSmthTag", AskDrink.topic_name)

            logger.log('Topic "' + AskDrink.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        if hasattr(AskDrink, 'action_id'):
            if hasattr(AskDrink, 'drinks'):
                for drink in AskDrink.drinks:
                    if data['data'] == drink['name']:
                        return {'id': AskDrink.action_id, 'drink': drink}
        return True

    @staticmethod
    def stop(local_manager):
        if hasattr(AskDrink, 'topic_name') and AskDrink.topic_name:
            local_manager.dialog.deactivateTopic(AskDrink.topic_name)
            local_manager.dialog.unloadTopic(AskDrink.topic_name)
            if hasattr(AskDrink, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(AskDrink, 'reactivateMovement')
            delattr(AskDrink, "topic_name")