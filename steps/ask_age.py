import json
import os
from tools import logger
from tools import arg_fetcher


class AskAge:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        AskAge.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not AskAge.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')

        name = arg_fetcher.get_argument(speech, 'name')

        text = text.format(name=name)
        said = said.format(name=name)

        ages = [str(i) for i in range(99)]

        if text:
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                'data': {
                    'textToShow': text
                }
            }))
        
        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)

            if arg_fetcher.get_argument(speech, 'noSpeechRecognition'):
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['error'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                AskAge.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(AskAge.topic_name)

                local_manager.dialog.activateTag("saySmthTag", AskAge.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", AskAge.topic_name)

            else:
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['ok'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['ask_smth'])

                local_manager.dialog.setConcept("askSmthDyn", "enu", [said])
                local_manager.dialog.setConcept("choices", "enu", ages)

                AskAge.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(AskAge.topic_name)

                local_manager.dialog.activateTag("askSmthTag", AskAge.topic_name)
                local_manager.dialog.gotoTag("askSmthTag", AskAge.topic_name)

            logger.log('Topic "' + AskAge.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        try:
            data['data'] = int(data['data'])
        except ValueError:
            pass
        if hasattr(AskAge, 'action_id') and type(data['data']) == int:
            return {'id': AskAge.action_id, 'age': data['data']}
        return True

    @staticmethod
    def stop(local_manager):
        if hasattr(AskAge, 'topic_name') and AskAge.topic_name:
            local_manager.dialog.deactivateTopic(AskAge.topic_name)
            local_manager.dialog.unloadTopic(AskAge.topic_name)
            if hasattr(AskAge, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(AskAge, 'reactivateMovement')
            delattr(AskAge, "topic_name")