import json
import os
from tools import logger
from tools import arg_fetcher


class AskOpenDoor:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        AskOpenDoor.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not AskOpenDoor.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')
        desc = arg_fetcher.get_argument(speech, 'description')
        if not desc:
            desc = ""
        

        if text:
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                'data': {
                    'textToShow': {
                        'title': text,
                        'description': [desc]
                    }
                }
            }))


        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                AskOpenDoor.reactivateMovement = True

            if arg_fetcher.get_argument(speech, 'noSpeechRecognition'):
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['error'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                AskOpenDoor.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(AskOpenDoor.topic_name)

                local_manager.dialog.activateTag("saySmthTag", AskOpenDoor.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", AskOpenDoor.topic_name)

            else:
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['ok'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_catch_next'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                AskOpenDoor.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(AskOpenDoor.topic_name)
                
                local_manager.dialog.activateTag("saySmthTag", AskOpenDoor.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", AskOpenDoor.topic_name)
            
            logger.log('Topic "' + AskOpenDoor.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        if hasattr(AskOpenDoor, 'action_id'):
            return {'id': AskOpenDoor.action_id}
        return True

    @staticmethod
    def stop(local_manager):
        if hasattr(AskOpenDoor, 'topic_name') and AskOpenDoor.topic_name:
            local_manager.dialog.deactivateTopic(AskOpenDoor.topic_name)
            local_manager.dialog.unloadTopic(AskOpenDoor.topic_name)
            if hasattr(AskOpenDoor, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(AskOpenDoor, 'reactivateMovement')
            delattr(AskOpenDoor, "topic_name")