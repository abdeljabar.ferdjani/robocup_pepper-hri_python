import json
import os
from tools import logger
from tools import arg_fetcher


class Confirm:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        Confirm.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not Confirm.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')

        if text and "{name}" in text or said and "{name}" in said:
            name = arg_fetcher.get_argument(speech, 'name')
            if text:
                text = text.format(name=name)
            if said:
                said = said.format(name=name)
        elif text and "{drink}" in text or said and "{drink}" in said:
            drink = arg_fetcher.get_argument(speech, 'drink')
            if text:
                text = text.format(drink=drink)
            if said:
                said = said.format(drink=drink)
        elif text and "{age}" in text or said and "{age}" in said:
            age = arg_fetcher.get_argument(speech, 'age')
            if text:
                text = text.format(age=age)
            if said:
                said = said.format(age=age)
    
        if text:
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                'data': {
                    'textToShow': text
                }
            }))

        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                Confirm.reactivateMovement = True

            if arg_fetcher.get_argument(speech, 'noSpeechRecognition'):
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['error'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                Confirm.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(Confirm.topic_name)

                local_manager.dialog.activateTag("saySmthTag", Confirm.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", Confirm.topic_name)

            else:
                toolbar = local_manager.lm_config['toolbarState']    
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['ok'],
                    'system': toolbar['system']['micro']
                }))
                
                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['confirm'])

                local_manager.dialog.setConcept("askConfirmDyn", "enu", [said])

                Confirm.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(Confirm.topic_name)

                local_manager.dialog.activateTag("askConfirm", Confirm.topic_name)
                local_manager.dialog.gotoTag("askConfirm", Confirm.topic_name)

            logger.log('Topic "' + Confirm.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        data = data['data']
        if hasattr(Confirm, 'action_id'):
            if type(data) is bool:
                return {'id': Confirm.action_id, 'confirm': data}
            elif data == "True" or data == "False":
                return {'id': Confirm.action_id, 'confirm': data == "True"}

    @staticmethod
    def stop(local_manager):
        if hasattr(Confirm, 'topic_name') and Confirm.topic_name:
            local_manager.dialog.deactivateTopic(Confirm.topic_name)
            local_manager.dialog.unloadTopic(Confirm.topic_name)
            if hasattr(Confirm, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(Confirm, 'reactivateMovement')
            delattr(Confirm, "topic_name")