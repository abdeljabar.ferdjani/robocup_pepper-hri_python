import json
import os
from tools import logger
from tools import arg_fetcher


class Wait:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        Wait.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not Wait.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')

        time = arg_fetcher.get_argument(args, 'time')
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')
        
        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                Wait.reactivateMovement = True
            
            top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])
            Wait.topic_name = local_manager.dialog.loadTopic(top_path)
 
        if text:
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                'data': {
                    'textToShow': text,
                    'time' : time
                }
            }))

        if said:
            toolbar = local_manager.lm_config['toolbarState']
            local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                'state': toolbar['state']['ok'],
                'system': toolbar['system']['micro']
            }))
            
            local_manager.dialog.setConcept("saySmthDyn", "enu", [said])
            
            local_manager.dialog.activateTopic(Wait.topic_name)
            
            local_manager.dialog.activateTag("saySmthTag", Wait.topic_name)
            local_manager.dialog.gotoTag("saySmthTag", Wait.topic_name)
            
            logger.log('Topic "' + Wait.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        if hasattr(Wait, 'action_id') and data['data'] != "1":
            return {'id': Wait.action_id}
        return False

    @staticmethod
    def stop(local_manager):
        if hasattr(Wait, 'topic_name') and Wait.topic_name:
            local_manager.dialog.deactivateTopic(Wait.topic_name)
            local_manager.dialog.unloadTopic(Wait.topic_name)
            if hasattr(Wait, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(Wait, 'reactivateMovement')
            delattr(Wait, "topic_name")