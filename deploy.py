#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

import argparse
import os
import subprocess

# import getpass

os.chdir(os.path.dirname(os.path.realpath(__file__)))  # Set working directory to script directory
parser = argparse.ArgumentParser("Deploy to Pepper")

parser.add_argument("--ip", "-i",
                    type=str,
                    help="Ip of Pepper",
                    required=True,
                    dest="ip")

parser.add_argument("--user", "-u",
                    type=str,
                    help="User for Pepper",
                    dest="user",
                    default="nao")

parser.add_argument("--app", "-a",
                    type=str,
                    help="Application name",
                    required=False, dest="app",
                    default="R2019")

parser.add_argument("--folder", "-f_name",
                    type=str,
                    help="Python folder in app",
                    required=False, dest="folder",
                    default="python")

parser.add_argument("--path", "-p",
                    type=str,
                    help="Path to application",
                    required=False,
                    dest="path",
                    default="/home/nao/.local/share/PackageManager/apps")


def create_str(user, ip):
    return "{0}@{1}".format(user, ip)


if __name__ == '__main__':
    args = parser.parse_args()

    try:
        input = raw_input
    except NameError:
        pass
    # Now input will return a string in Python 2 as well

    str_pepper = create_str(args.user, args.ip)
    print(str_pepper)
    # password = getpass.getpass()

    # Push files
    path_on_pepper = "{0}/{1}/{2}".format(args.path, args.app, args.folder)

    print("Creating tar archive from local files...")
    print(" ".join(["tar", "-czf", "python.tar.gz", "--exclude-vcs-ignores", "--exclude-vcs", "."]))

    subprocess.call(["tar", "-czf", "python.tar.gz", "--exclude-vcs-ignores", "--exclude-vcs", "."])
        # print(
        #     "Something went wrong while executing previous command, please execute it in an other terminal and press enter when done")
        # input()
    print("Done")

    print("Creating remote directory " + path_on_pepper)
    # subprocess.call(["ssh", "-t", "{0}:{1}@{2}".format(args.user, password, args.ip), "mkdir -p {0}".format(path_on_pepper)], shell=False)

    print(" ".join(["ssh", "-t", str_pepper, "mkdir -p {0} && rm -rf {0}/*".format(path_on_pepper)]))
    if subprocess.call(["ssh", "-t", str_pepper, "mkdir -p {0} && rm -rf {0}/*".format(path_on_pepper)], shell=False) != 0:
        print(
            "Something went wrong while executing previous command, please execute it in an other terminal and press enter when done")
        input()
    print("Done")

    print("Sending tar archive to remote location")
    print(" ".join(["scp", "./python.tar.gz", str_pepper + ':' + path_on_pepper]))

    if subprocess.call(" ".join(["scp", "./python.tar.gz", str_pepper + ':' + path_on_pepper]), shell=True) != 0:
        print(
            "Something went wrong while executing previous command, please execute it in an other terminal and press enter when done")
        input()
    print("Done")

    print("Extracting tar archive to remote directory")
    print(" ".join(
        ["ssh", "-t", str_pepper, "tar xzf {0}/python.tar.gz -C {0} && rm {0}/python.tar.gz".format(path_on_pepper)]))

    # subprocess.call(["ssh", "-t", "{0}:{1}@{2}".format(args.user, password, args.ip), "tar xzvf {0}/python.tar.gz".format(path_on_pepper)], shell=False)
    if subprocess.call(["ssh", "-t", str_pepper,
                        "tar xzf {0}/python.tar.gz -C {0} && rm {0}/python.tar.gz".format(path_on_pepper)],
                       shell=False) != 0:
        print(
            "Something went wrong while executing previous command, please execute it in an other terminal and press enter when done")
        input()
    print("Done")

    print("Deleting tar archive from local files...")
    print(" ".join(["rm", "python.tar.gz"]))

    if subprocess.call(["rm", "python.tar.gz"]) != 0:
        print(
            "Something went wrong while executing previous command, please execute it in an other terminal and press enter when done")
        input()
    print("Done")

    # print("Allowing to execute ./local_manager.py")
    # print(" ".join(["ssh", "-t", str_pepper, "chmod +x {0}/{1}".format(path_on_pepper, "local_manager.py")]))

    # # subprocess.call(["ssh", "-t", "{0}:{1}@{2}".format(args.user, password, args.ip), "chmod +x {0}/{1}".format(path_on_pepper, "local_manager.py")], shell=False)
    # if subprocess.call(["ssh", "-t", str_pepper, "chmod +x {0}/{1}".format(path_on_pepper, "local_manager.py")], shell=False) != 0:
    #     print("Something went wrong while executing previous command, please execute it in an other terminal and press enter when done")
    #     input()
    # print("Done")
