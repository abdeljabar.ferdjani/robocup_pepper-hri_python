#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

import qi
import sys
import argparse
import json
import time
from tools import logger
from tools.ApiWrapper.wrapper import Wrapper
import os
from datetime import datetime
from threading import Thread
from steps.views import Views

app_name = "Local Manager"
web_app_name = "R2019"

os.chdir(os.path.dirname(os.path.realpath(__file__))) # Set working directory to script directory

class LocalManager:
    def __init__(self, app, lm_config, launch_app, launch_qichat, scenario, noSpeechMove):
        self.app = app
        app.start()
        self.session = app.session
        self.memory = self.session.service("ALMemory")
        self.dialog = self.session.service("ALDialog")
        self.autonomous_life = self.session.service("ALAutonomousLife")
        if noSpeechMove:
            self.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
        self.lm_config = lm_config
        self.scenario = scenario
        self.steps = {}

        # send config and setup files into ALMemory for webApp
        with open(self.lm_config['jsons']['apis']['common']) as common_al_file:
            al_mems_to_send = json.load(common_al_file)['AL_VALUE']
            self.send_almem_configs(self.lm_config['jsons'], al_mems_to_send)
        
        self.wrapper = Wrapper(self)
        
        self.run_check_js_heartbeat = False
        self.run_check_gm_heartbeat = False

        self.callback_ids = {}
        self.events_subscriber()

        self.nb_tries = 0
        if launch_app:
            self.start_web_interface()
        else:
            logger.log("Not starting tablet application (--no-app)", app_name, logger.WARNING)

        self.launch_qichat = launch_qichat
        if launch_qichat:
            self.start_qichat()
        else:
            logger.log("Not starting speech recognition (--no-qichat)", app_name, logger.WARNING)

        self.run_send_lm_heartbeat = True
        t = Thread(target=self.send_lm_heartbeat)
        t.start()


    def __enter__(self):
        return self

    # region START TABLET APPLICATION

    def send_almem_configs(self, file_paths, al_mems, add_to_lm_config=False):
        for json_key in file_paths: # iterate on keys of files to send
            try:
                if json_key == "apis":
                    self.send_almem_configs(file_paths[json_key], al_mems[json_key], True)
                else:
                    with open(file_paths[json_key]) as file:
                        content = file.read()
                        self.memory.insertData(al_mems[json_key], content)
                        if add_to_lm_config: # add api keys to self.lm_config
                            content = json.loads(content)
                            for key in content:
                                self.lm_config[key] = content[key]
                    logger.log("Sent " + json_key + ".json into ALMemory",
                                app_name, logger.DEBUG)
            except Exception as ex:
                logger.log(
                    'An error occured while opening file "' + file_paths[json_key] + '": ' + str(ex),
                    app_name, logger.ERROR)
        self.memory.insertData(self.lm_config['AL_VALUE']['guests'],
                               json.dumps({'John': {'name': "John", 'drink': "Coke"}}))


    def start_web_interface(self):
        try:
            # start webApp
            logger.log("Starting tablet application...", app_name, logger.DEBUG)
            tablet_service = self.app.session.service("ALTabletService")
            tablet_service.wakeUp()
            # time.sleep(1)
            # try to load webApp; if sucessful, try to show webApp
            app_started = tablet_service.loadApplication(
                web_app_name) and tablet_service.showWebview()
            if app_started:
                logger.log("Tablet application launched successfully", app_name)
            else:
                if self.nb_tries < 5:  # try to launch up to 5 times
                    self.nb_tries += 1
                    logger.log(
                        "Something went wrong while starting tablet application, retrying...", app_name, logger.ERROR)
                    self.start_web_interface()
                    return
                else:
                    logger.log('Error while starting tablet application, please check app name (is it "' +
                               web_app_name + '"?)', app_name, logger.CRITICAL)
                    sys.exit(1)

        except Exception as e:
            logger.log("Error was: " + str(e), app_name, logger.ERROR)

    # endregion START TABLET APPLICATION

    # region START QICHAT (.TOP)

    def start_qichat(self):
        logger.log("Starting QiChat", "QiChat", logger.INFO)
        self.dialog.resetAll()
        self.dialog.setLanguage("English")
        self.dialog.subscribe(app_name)

        try:
            for top in self.dialog.getAllLoadedTopics():
                self.dialog.unloadTopic(top)
        except RuntimeError as ex:
            logger.log("Exception while unloading topic: " + str(ex), app_name, logger.ERROR)
        
        logger.log("Verifying topics (pre-loading)...", "QiChat", logger.DEBUG)
        missing_topics = []
        for top in self.lm_config['tops'].values():
            top_path = os.path.join(os.getcwd(), top)
            try:
                topic = self.dialog.loadTopic(top_path)
                logger.log('Loaded "' + top + '" topic successfully', "QiChat", logger.DEBUG)
                self.dialog.unloadTopic(topic)
            except RuntimeError as ex:
                logger.log('Could not load top file "' + top_path + '":\n' + str(ex), "QiChat", logger.CRITICAL)
                missing_topics.append(top)
            
        logger.log("QiChat started", "QiChat", logger.INFO)
        for missing in missing_topics:
            logger.log('Missing topic: "' + missing +
                       '" (check file path in "json/localManager.json" -> tops and .top syntax in "' + top + '")',
                       "QiChat", logger.CRITICAL)

    # endregion START QICHAT (.TOP)

    # region LOAD SCENARIO

    def load_scenario(self, scenario):
        # with open(self.lm_config['jsons']['scenario']) as file:
        #     try:
        #         scenarii = json.load(file)
        #         if scenario in scenarii:
        #             self.memory.raiseEvent(self.lm_config['currentScenario']['ALMemory'],
        #                                    json.dumps({'scenario': scenarii[scenario]}))
        #             self.steps = {i['id']: i for i in scenarii[scenario]['steps']}
        #     except Exception as ex:
        #         logger.log("Error while sending scenario: " +
        #                    str(ex), app_name, logger.ERROR)
        self.scenario = scenario
        self.memory.raiseEvent(self.lm_config['askToChangeScenario']['ALMemory'], json.dumps({'scenario': scenario}))

    # endregion LOAD SCENARIO

    # region EVENT METHODS

    def events_subscriber(self):
        # DEBUG
        #self.attach_event("TouchChanged", log=True)
        self.attach_event("Dialog/LastInput", log=True)
        self.attach_event("Dialog/Answered", log=True)
        #self.attach_event("R2019/Global/TimerState", log=True)
        self.attach_event(self.lm_config['currentView']['ALMemory'], log=True)
        # FROM JS
        self.attach_event(self.lm_config['dataJs']['ALMemory'], self.received_data_js, log=True)
        self.attach_event(self.lm_config['tabletOperational']['ALMemory'], self.js_started)
        self.attach_event(self.lm_config['jsHeartbeat']['ALMemory'], self.js_heartbeat)
        self.attach_event(self.lm_config['askToChangeScenario']['ALMemory'], self.wrapper.ask_for_scenario)
        self.attach_event(self.lm_config['localManagerLogger']['ALMemory'], self.local_manager_logger)
        # FROM GM
        self.attach_event(self.lm_config['dataPython']['ALMemory'], self.received_data_python, log=True)
        self.attach_event(self.lm_config['currentAction']['ALMemory'], self.change_action, log=True)
        self.attach_event(self.lm_config['generalManagerHeartbeat']['ALMemory'], self.gm_heartbeat, log=True)
        # self.attach_event(self.lm_config['currentScenario']['ALMemory'], self.change_scenario, log=True)

    def attach_event(self, event_name, callback=None, log=False):
        try:
            if callback is not None or log:
                self.callback_ids[event_name] = self.memory.subscriber(event_name)
            if callback is not None:
                self.callback_ids[event_name].signal.connect(callback)
                logger.log("Attached event " + event_name + " to " +
                        callback.__name__, app_name, logger.DEBUG)
            if log:
                self.callback_ids[event_name].signal.connect(
                    lambda event: self.print_event_value(event, event_name))
                logger.log("Attached event " + event_name + " to print_event_value", app_name, logger.DEBUG)
        except Exception as ex:
            logger.log("Something went wrong while attaching event " +
                       event_name + ": " + str(ex), app_name, logger.ERROR)
    
    # endregion EVENT METHODS

    # region HEARTBEATS

    def check_js_heartbeat(self):
        while self.run_check_js_heartbeat:
            # check JS heartbeat
            delta = datetime.now() - self.last_js_heartbeat
            if delta.seconds > 1:
                log_level = logger.WARNING
                if delta.seconds > 4:
                    log_level = logger.ERROR
                if delta.seconds > 9:
                    log_level = logger.CRITICAL
                logger.log("Did not received JS heartbeat for {} seconds".format(delta.seconds), app_name, log_level)
            time.sleep(.001)

    def check_gm_heartbeat(self):
        while self.run_check_gm_heartbeat:
            # check GM heartbeat
            delta = datetime.now() - self.last_gm_heartbeat
            if delta.seconds >= 1:
                log_level = logger.WARNING
                self.session.service("ALMotion").stopMove() # TODO Benoit check if it works to stop moving
                if delta.seconds >= 2:
                    Views.start("goTo", self, {
                        'args': {
                            'speech': {
                                'title': "Connection lost with the General Manager", 
                                'said': "I lost connection with the General Manager"
                            },
                            'location': {
                                'pathOnTablet': "img/images/gm_lost.gif"
                            }
                        }
                    })
                    log_level = logger.ERROR
                if delta.seconds >= 4:
                    log_level = logger.CRITICAL
                logger.log("Did not received GM heartbeat for {} seconds".format(delta.seconds), app_name, log_level)
                if log_level >= logger.ERROR:
                    time.sleep(4.9)
            time.sleep(.2)

    def send_lm_heartbeat(self):
        while self.run_send_lm_heartbeat:
            # send python heartbeat
            self.memory.raiseEvent(self.lm_config['localManagerHeartbeat']['ALMemory'],
                                   json.dumps({'time': int(time.mktime(datetime.now().timetuple()))}))
            logger.log("Sent local manager heartbeat (" + str(int(time.mktime(datetime.now().timetuple())))
                       + ")", app_name, logger.HEARTBEAT)
            time.sleep(1)
        
    # endregion HEARTBEATS

    # region CALLBACKS

    def print_event_value(self, value, key=None):
        logger.log(('Event from "' + str(key) + '": "', 'Event received: "')
                   [key is None] + str(value) + '"', app_name, logger.EVENT)

    def change_scenario(self, scenario):
        try:
            scenario = json.loads(scenario)['scenario']
            self.steps = {i['id']: i for i in scenario['steps']}
            logger.log('Successfully loaded scenario "' + scenario['name'] + '"', app_name, logger.INFO)
        except KeyError:
            logger.log("Error in change_scenario: expected a JSON with 'scenario' but found \""
                       + scenario + '"', app_name, logger.ERROR)

    # def change_action(self, arg):
    #     try:
    #         step_id = json.loads(arg)['actionId']
    #     except KeyError as ex:
    #         logger.log("Error in change_action: expected a JSON with 'actionId' but found \"" + arg + '"',
    #                   app_name, logger.ERROR)
    #         return
    #     if not self.steps:
    #         if self.scenario:
    #             self.load_scenario(self.scenario)
    #             logger.log("No scenario was loaded, please retry again", app_name, logger.WARNING)
    #         else:
    #             logger.log("Enable to change step, no scenario to load (try loading a scenario before loading an action)",
    #                        app_name, logger.ERROR)
    #     else:
    #         if step_id in self.steps:
    #             try:
    #                 logger.log("change_action: Arguments: " + str(arg), app_name, logger.EVENT)
    #                 Views.start(step_id, self, json.loads(arg))
    #             except KeyError as ex:
    #                 logger.log("Error: " + str(ex), app_name, logger.ERROR)

    def change_action(self, action_name, action_json):
        action = json.loads(action_json)
        logger.log("change_action: Arguments: " + str(action), app_name, logger.EVENT)
        Views.start(action_name, self, action)

    def send_view_result(self, action_name, result):
        self.wrapper.send_view_result(action_name, result)

 
    def received_data_js(self, value):
        try:
            Views.received_data(self, json.loads(value))
        except ValueError:
            logger.log('Received_data_js: Could not deserialize json ("' + value + '")', app_name, logger.ERROR)

    def received_data_python(self, value):
        try:
            Views.received_data(self, {'data': value})
        except ValueError:
            logger.log('Received_data_python: Could not deserialize json ("' + value + '")', app_name, logger.ERROR)

    def js_started(self, timestamp):
        logger.log("Tablet application started on " + str(datetime.fromtimestamp(
            json.loads(timestamp)['time']/1000)), app_name, logger.DEBUG)

        if not self.run_check_js_heartbeat:
            self.last_js_heartbeat = datetime.now()
            self.run_check_js_heartbeat = True
            t = Thread(target=self.check_js_heartbeat)
            t.start()

    def gm_heartbeat(self, timestamp):
        self.last_gm_heartbeat = datetime.now()
        logger.log("GM heartbeat received", "Heartbeat", logger.HEARTBEAT)
        if not self.run_check_gm_heartbeat:
            self.run_check_gm_heartbeat = True
            t = Thread(target=self.check_gm_heartbeat)
            t.start()

    def js_heartbeat(self, timestamp):
        self.last_js_heartbeat = datetime.now()
        logger.log("Tablet heartbeat received", "Heartbeat", logger.HEARTBEAT)

    def local_manager_logger(self, log):
        try:
            log = json.loads(log)
        except ValueError:
            logger.log('Malformed log received: "' +
                       log + '"', "Logger", logger.WARNING)
            return
        try:
            logger.log(log['message'], log['application'],
                       log['level'] if 'level' in log else logger.INFO)
        except:
            logger.log('Error logging message "' + str(log) +
                       '"', "Logger", logger.WARNING)

    # endregion CALLBACKS
    
    # region EXIT 

    def __exit__(self, ex_type, value, traceback):
        self.run_check_js_heartbeat = False
        self.run_check_gm_heartbeat = False
        self.run_send_lm_heartbeat = False
        
        if self.launch_qichat:
            Views.stop(self)
            self.dialog.unsubscribe(app_name)
            logger.log("Stopped QiChat", "QiChat", logger.INFO)
    
    # endregion EXIT

# region MAIN

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
    parser.add_argument("--no-app", action="store_true",
                        help="Only launch local manager (and qichat), without tablet application")
    parser.add_argument("--no-qichat", action="store_true",
                        help="Only launch local manager (and tablet), without speech recognition")
    parser.add_argument("--almemory", action="store_true",
                        help="Only send jsons in ALMemory to make tablet application work, and exit")
    parser.add_argument("--log-level", type=str, default="DEBUG",
                        help="Use to print only logs above choosen level: "
                           + "[<HEARTBEAT> <EVENT> <DEBUG> <INFO> <WARNING> <ERROR> <CRITICAL>]")
    parser.add_argument("--scenario", type=str, default="None",
                        help="Select scenario to launch on startup: [<servingDrinks> <receptionist>]")
    parser.add_argument("--no-speech-move", dest="noSpeechMove", action="store_true",
                        help="Disables autonomous movement when speaking for a whole scenario")
                        
    args = parser.parse_args()

    log_levels = ("HEARTBEAT", "EVENT", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL")
    if args.log_level in log_levels:
        logger.set_print_level(log_levels.index(args.log_level))

    logger.log("Starting local manager...", app_name)

    try:
        # Initialize qi framework.
        logger.log("Initializing qi framework...", app_name, logger.DEBUG)
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(
            [app_name, "--qi-url=" + connection_url])
        logger.log("Qi framework connected", app_name, logger.DEBUG)
    except RuntimeError:
        logger.log("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) + ".\n"
                   "Please check your script arguments. Run with -h option for help.", app_name, logger.CRITICAL)
        sys.exit(1)

    with open(os.path.join("api", "localManager.json")) as file:
        lm_config = json.load(file)
    logger.log("Loaded JSON config (api/common.json)",
               app_name, logger.DEBUG)

    if args.almemory:
        args.no_app = True
        args.no_qichat = True


    with LocalManager(app, lm_config, not args.no_app, not args.no_qichat, args.scenario, args.noSpeechMove) as local_manager:
        try:
            if not args.almemory:
                app.run()
        except Exception as ex:
            logger.log("Error was: " + str(ex), app_name, logger.WARNING)
        finally:
            app.stop()
    
    app.session.service("ALTabletService").wakeUp()
    logger.log("Local manager terminated", app_name, logger.INFO)

# endregion MAIN