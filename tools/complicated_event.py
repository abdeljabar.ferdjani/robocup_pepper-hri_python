import os
import qi
import sys
import argparse


class MemoryEventCatcher(object):
    def __init__(self, app):
        super(MemoryEventCatcher, self).__init__()
        print "starting"
        app.start()
        session = app.session
        self.memory = session.service("ALMemory")
        self.touch = self.memory.subscriber("TouchChanged")
        self.id = self.touch.signal.connect(self.on_event)
        
    def on_event(self, value):
        print value


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
    args = parser.parse_args()
    try:
        # Initialize qi framework.
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["MemoryEventCatcher", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    wifi_test = MemoryEventCatcher(app)
    app.run()
