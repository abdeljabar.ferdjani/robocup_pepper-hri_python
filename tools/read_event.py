import sys
from naoqi import ALProxy
import json

eventName = "Mt"

if len(sys.argv) > 1:
	eventName = sys.argv[1]

mem = ALProxy("ALMemory", "localhost", 9559)

try:
	print(eventName + " (json) : " + json.dumps(json.loads(mem.getData(eventName)), indent=4))
except RuntimeError:
	sys.stderr.write("Error: key {" + eventName + "} not found\n")
	exit(1)
except Exception:
	print(eventName + " (plain text): " + str(mem.getData(eventName)))