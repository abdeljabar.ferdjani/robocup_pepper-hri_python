import sys
from naoqi import ALProxy

eventName = "Mt"
eventValue = 42
eventNb = 1

if sys.argv[1] == "-h":
	print("Usage: send_event.py <eventName> [eventValue] [eventNb]")

if len(sys.argv) > 1:
	eventName = sys.argv[1]
if len(sys.argv) > 2:
	eventValue = sys.argv[2]
if len(sys.argv) > 3:
	eventNb = int(sys.argv[3])

mem = ALProxy("ALMemory", "localhost", 9559)

for i in range(eventNb):
	mem.raiseEvent(eventName, eventValue)
	print(str(i) + ": Sent " + str(eventValue) + " on " + str(eventName))
