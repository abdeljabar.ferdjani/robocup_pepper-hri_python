import qi
import json
from tools import logger
import time
from datetime import datetime
from os import path
import uuid

app_name = "APIWrapper"
config_path = path.join(path.dirname(__file__), "wrapper_config.json")
log_file = path.join("logs", "wrapper" + datetime.now().strftime(
    "%Y-%m-%d_%H:%M:%S") + ".log")

class Wrapper:
    """Wrapper for RoboCup-HRI API
    Use this to communicate with the General Manager, 
    """

    def __init__(self, local_manager):
        """Constructor for Wrapper Class
        
        Arguments:
            naoqi_session {qi.Session} -- Session to get an ALMemory instance from
        """

        with open(config_path, 'r') as json_config:
            config = json.load(json_config)
        
            self.prefix = config['prefix']
            self.suffixes = config['suffixes']
            self.views = config['views']
            self.timeboard = config['timeboard']
            self.goals_to_gm = config['goalsToGM']

        self.ask_for_scenario_uuid = ""

        self.local_manager = local_manager
        self.memory = local_manager.memory
        self.callback_ids = {}
        self.subscribe_to_channels()


    def subscribe_to_channels(self):
        for suf in (self.suffixes['GOAL'], self.suffixes['RESULT']):
            for chan in self.views:
                self.attach_event(chan, suf, self.channels_callback)
            for chan in self.timeboard:
                self.attach_event(chan, suf, self.timeboard_callback)
        


    def channels_callback(self, value, key=None):
        logger.log("Received {0} from {1}".format(value, key), app_name, logger.DEBUG)
        if key:
            keys = key.split("/")
            if len(keys) < 3:
                pass # TODO add debug
            if len(keys) > 3:
                pass # TODO add debug

            suffix = keys[-1]
            logger.log(suffix, app_name, logger.DEBUG)
            if suffix in self.suffixes.values(): # last index (suffix), should be either 'goal', 'result', ...
                action = "/".join(keys[1:-1])
                logger.log(action, app_name, logger.DEBUG)
                if suffix == self.suffixes['GOAL']:
                    with open(log_file, "a") as f:
                        f.write("python tools/send_event.py {0} \"{1}\"\n".format(key, value.replace('"', '\\"')))
                    self.memory.insertData(key, json.dumps(dict()))
                    self.local_manager.change_action(action, value)
    

    def timeboard_callback(self, value, key=None):
        logger.log("Received " + value + " on " + key, app_name, logger.DEBUG)
        keys = key.split("/")
        suffix = keys[-1]
        if suffix == self.suffixes['GOAL']:
            with open(log_file, "a") as f:
                f.write("python tools/send_event.py {0} \"{1}\"\n".format(key, value.replace('"', '\\"')))
            value = json.loads(value)
            print(str(value))
            self.memory.raiseEvent("{0}/{1}/{2}".format(self.prefix, "Global", "/".join(keys[1:-1])), json.dumps(value['args']))
            logger.log("Sent " + json.dumps(value) + " on " + "{0}/{1}/{2}".format(keys[0], "Global", "/".join(keys[1:-1])), app_name, logger.DEBUG)
            self.memory.insertData(key, json.dumps(dict()))
            self.send_view_result("/".join(keys[1:-1]), {'id': value['id']})


    def goals_to_gm_callback(self, value, key=None):
        logger.log("Received {0} from {1}".format(value, key), app_name, logger.DEBUG)


    def send_view_result(self, view_name, result):
        result_chan = "{0}/{1}/{2}".format(self.prefix, view_name, self.suffixes['RESULT'])
        #if not self.local_manager.memory.getData(result_chan):
        result['timestamp'] = time.time()
        self.memory.raiseEvent(result_chan, json.dumps(result))
        logger.log("Sent result " + json.dumps(result) + " on " + result_chan, app_name, logger.DEBUG)

    
    def ask_for_scenario(self, scenario_json):
        scenario_id = json.loads(scenario_json)
        logger.log("Received a scenario request ({0}) from tablet".format(scenario_id), app_name, logger.DEBUG)

        if self.ask_for_scenario_uuid: # Already waiting for a scenario
            logger.log("Already waiting for a scenario", app_name, logger.DEBUG)
            return
        
        self.ask_for_scenario_uuid = str(uuid.uuid4())
        data = {
            'id': self.ask_for_scenario_uuid,
            'timestamp': time.time(),
            'args': scenario_id
        }
        
        chan = "{0}/{1}/{2}".format(self.prefix, self.goals_to_gm['askChangeScenario'], self.suffixes['GOAL'])
        self.memory.raiseEvent(chan, json.dumps(data))
        logger.log("Sent {0} on {1}".format(json.dumps(data), chan), app_name, logger.DEBUG)

        received_data = self.wait_for_result(self.goals_to_gm['askChangeScenario'], self.ask_for_scenario_uuid)
        self.ask_for_scenario_uuid = ""
        

    def wait_for_result(self, action_name, action_id):
        chan = "{0}/{1}/{2}".format(self.prefix, action_name, self.suffixes['RESULT'])
        logger.log("Waiting for result on {0}".format(chan), app_name, logger.DEBUG)

        while True:
            data = {}
            try:
                data = json.loads(self.memory.getData(chan))
            except RuntimeError:
                pass # This channel needs to be created by the GM
            if 'id' in data.keys() and data['id'] == action_id:
                logger.log("Received result {0} from {1}".format(data, chan), app_name, logger.DEBUG)
                self.memory.insertData(chan, json.dumps(dict()))
                return data

    def attach_event(self, event_name, suffix, callback):
        try:
            curr_chan = "{0}/{1}/{2}".format(self.prefix, event_name, suffix)
            self.local_manager.memory.insertData(curr_chan, json.dumps(dict()))
            self.callback_ids[curr_chan] = self.memory.subscriber(curr_chan)
            self.callback_ids[curr_chan].signal.connect(lambda event: callback(event, curr_chan))
            logger.log("Attached event " + curr_chan + " to " + callback.__name__, app_name, logger.DEBUG)
        except Exception as ex:
            logger.log("Something went wrong while attaching event " + curr_chan + ": " + str(ex), app_name, logger.ERROR)